﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    float timeAux;
    //public float VelocidadReaparicion;
    public static int LimiteEnemigos;
    public bool Generar;
    public bool Maximo;
    public static bool Derrota = false;

    //Listas de objetos.
    public GameObject Righter;
    public List<GameObject> EnemigosAB;


    //Lista de vectores de reaparición.
    public List<Vector3> Camino = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {
        timeAux = Time.time;
        //VelocidadReaparicion = 2.5f;
        LimiteEnemigos = 0;
        Generar = true;

        Maximo = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Derrota == false)
        {
            //Genera un límite de enemigos para hacer que Unity mínimo vaya menos lento.

            if (Generar && LimiteEnemigos <= 100)
            {
                //Corrutina para general enemigos.
                StartCoroutine(Spawneres());
            }

            else
            {
                Generar = false;
            }
        }


        //float timeDif = Time.time - timeAux;




        //Mismo sistema de aparición de otros trabajos, un objeto oculto y cada rato van apareciendo otro.
        //Lo mantengo debido a que era una prueba, funciona pero es más compacto el otro y más eficaz.

        /*if (timeDif > VelocidadReaparicion)
        {

            //Crear clones e instanciarlos en el Spawner.
            GameObject clone = Instantiate(Enemigo, pos, Quaternion.identity) as GameObject;
                clone.SetActive(true);

                timeAux = Time.time;
                
        }*/
    }

    IEnumerator Spawneres()
    {
        //Corrutina para general enemigos.

        Generar = false;
        GameObject Enemigos;

        Enemigos = Instantiate(EnemigosAB[Random.Range(0, EnemigosAB.Count)], Camino[Random.Range(0, Camino.Count)], Quaternion.identity) as GameObject;
        //Activo el clon;
        Enemigos.SetActive(true);

        //Cada 3 segundos genera un enemigo.
        yield return new WaitForSeconds(3f);
        Generar = true;
        LimiteEnemigos++;
    }
}
