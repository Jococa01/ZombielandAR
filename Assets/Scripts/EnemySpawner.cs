﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public float VelocidadReaparicion;
    public static int LimiteEnemigos;
    public bool Generar;
    public bool Maximo;

    //Listas de objetos.
    public List<GameObject> EnemigosAB;


    //Lista de vectores de reaparición.
    public List<Vector3> Camino = new List<Vector3>();

    // Start is called before the first frame update
    void Start()
    {
        Generar = true;

    }

    // Update is called once per frame
    void Update()
    {
        //Genera un límite de enemigos para hacer que Unity mínimo vaya menos lento.

        if (Generar)
        {
            //Corrutina para general enemigos.
            StartCoroutine(Spawneres());
        }

        else
        {
            Generar = false;
        }
    }



    IEnumerator Spawneres()
    {
        //Corrutina para general enemigos.

        Generar = false;
        GameObject Enemigos;
        Enemigos = Instantiate(EnemigosAB[Random.Range(0, EnemigosAB.Count)], Camino[Random.Range(0, Camino.Count)], Quaternion.identity) as GameObject;
        //Activo el clon;
        Enemigos.SetActive(true);

        //Cada  segundos genera un enemigo.
        yield return new WaitForSeconds(VelocidadReaparicion);
        Generar = true;
    }
}

