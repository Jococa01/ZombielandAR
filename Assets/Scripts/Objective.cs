﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Objective : MonoBehaviour
{
    public GameObject HPBar, cam;
    public static int HP=40;
    public Text VidaBase;
    public int VidaIni;
    // Start is called before the first frame update
    void Start()
    {
        VidaIni = 40;
    }

    // Update is called once per frame
    void Update()
    {
        VidaBase.text = (HP + "/" + VidaIni);
        HPBar.transform.LookAt(cam.transform.transform.position);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemigo")
        {
            HP--;
            Destroy(other.gameObject);
        }
    }

}
