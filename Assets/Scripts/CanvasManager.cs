﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanvasManager : MonoBehaviour
{
    public static bool AudioActive = true;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Quit()
    {
        Application.Quit();
    }
    public void WeaponPistol()
    {
        TouchManager.WeaponType = 0;
    }
    public void WeaponUzi()
    {
        TouchManager.WeaponType = 1;
    }
    public void WeaponSniper()
    {
        TouchManager.WeaponType = 2;
    }
    public void AudioEnabled()
    {
        AudioActive = !AudioActive;
    }
}
