﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyBehaviour : MonoBehaviour
{
    public int EnemyHP = 10;
    //-------LootProperties------------//
    public int RandomNumber;
    public bool HasLootAsigned = false;
    public GameObject LootA, LootB, LootC;

    public GameObject Objective;
    public Rigidbody EnemyRigy;
    //public NavMeshAgent EnemyNav;
    //Movement
    public Transform[] ObjectiveDir;
    public int Speed, RotSpeed = 10;
    private int Current;

    //Other

    Rigidbody rb;

    //--------------------------------//
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        RandomNumber = Random.Range(0, 100);
    }

    // Update is called once per frame
    void Update()
    {
        //------------------------------------EnemyHP----------------------------------//
        if (EnemyHP <= 0)
        {
            if (RandomNumber <= 40)
            {
                Debug.Log("Drop A");
                TouchManager.EnemiesEliminated++;
                GameObject clone = Instantiate(LootA, transform.position, transform.rotation);
                clone.SetActive(true);
                Destroy(gameObject);
            }
            if (RandomNumber >40 && RandomNumber<= 75)
            {
                Debug.Log("Drop B");
                TouchManager.EnemiesEliminated++;
                GameObject clone = Instantiate(LootB, transform.position, transform.rotation);
                clone.SetActive(true);
                Destroy(gameObject);
            }
            if(RandomNumber > 75)
            {
                Debug.Log("Drop C");
                TouchManager.EnemiesEliminated++;
                GameObject clone = Instantiate(LootC, transform.position, transform.rotation);
                clone.SetActive(true);
                Destroy(gameObject);
            }
        }
        
        
        if (transform.position != ObjectiveDir[Current].position)
            {
            transform.rotation = Quaternion.Slerp(transform.rotation
                                   , Quaternion.LookRotation(ObjectiveDir[Current].position - transform.position)
                                   , RotSpeed * Time.deltaTime);

            Vector3 Positions = Vector3.MoveTowards(transform.position, ObjectiveDir[Current].position, Speed * Time.deltaTime);
                rb.MovePosition(Positions);
            }

            else Current = (Current + 1) % ObjectiveDir.Length;
        //-------------------------------------------------------------------------------//

        //-----------------------------------Direction-------------------------------------//
        //transform.position = Vector3.MoveTowards(transform.position,Objective.transform.position,0.0001f);
        /*EnemyNav.SetDestination(Objective.transform.position);
        EnemyNav.Move(transform.forward * Time.deltaTime * .001f);*/
    }
    //-------------------voids----------------------//
    public void lootable()
    {
        RandomNumber = Random.Range(0, 100);
        HasLootAsigned = true;
    }




    //Movimiento






}
   
