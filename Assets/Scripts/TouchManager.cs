﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchManager : MonoBehaviour
{
    //------------touch mechanics-------------//
    TouchPhase touchPhase = TouchPhase.Began;
    //---------------------------------------//

    //------------Scene objects--------------//
    Camera Maincam;
    //---------------------------------------//

    //--------------variables----------------//
    Vector3 FixedCenter;
    public static int WeaponType = 0;
    public static int AmmoPistol=36;
    public static int AmmoUzi = 0;
    public static int AmmoSniper = 0;
    public static int EnemiesEliminated = 0;
    public bool AbleToShootP = true;
    public int ClickCountP = 0;
    public bool AbleToShootU = true;
    public int ClickCountU = 0;
    public bool AbleToShootS = true;
    public int ClickCountS = 0;

    public bool ShootNextP = true;
    public bool ShootNextS = true;

    bool isFiring;
    bool stopFiring;
    public int RndNmb;

    public GameObject ShootB, HoldB;
    //---------------------------------------//

    private void Awake()
    {
        Maincam = gameObject.GetComponent<Camera>();
        FixedCenter = new Vector3(0.5f, 0.5f, 0);
    }

    void FixedUpdate()
    {
        //if (Input.touchCount == 1 && Input.GetTouch(0).phase == touchPhase)
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.green, 100f);
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("Tocado");
                if (hit.collider.tag == "loot1" || hit.collider.tag == "loot2" || hit.collider.tag == "loot2")
                {
                    Debug.Log("looting ammo");
                    if (hit.collider.tag == "loot1")
                    {
                        int RandomNMB = Random.Range(3, 12);
                        AmmoPistol += RandomNMB;
                        Destroy(hit.collider.gameObject);
                        Debug.Log("Conseguiste " + RandomNMB + "Balas de Pistola");
                    }
                    if (hit.collider.tag == "loot2")
                    {
                        int RandomNMB = Random.Range(10, 30);
                        AmmoUzi += RandomNMB;
                        Destroy(hit.collider.gameObject);
                        Debug.Log("Conseguiste " + RandomNMB + "Balas de Uzi");
                    }
                    if (hit.collider.tag == "loot3")
                    {
                        int RandomNMB = Random.Range(1,3);
                        AmmoSniper += RandomNMB;
                        Destroy(hit.collider.gameObject);
                        Debug.Log("Conseguiste " + RandomNMB+"Balas de Sniper");
                    }
                }
            }
        }
        //Enable / disable The hold function depending on the weapon type
        if (WeaponType != 1)
        {
            ShootB.SetActive(true);
            HoldB.SetActive(false);
        }
        else
        {
            ShootB.SetActive(false);
            HoldB.SetActive(true);
        }

        //Uzi Hold Fire Mechanic
        if (isFiring)
        {
            makeFireVariableFalse();
            Ray ray = Camera.main.ViewportPointToRay(FixedCenter);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 100f);
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("Tocado");
                if (hit.collider.tag == "Enemy")
                {
                    if (WeaponType == 1 &&ShootNextP==true)
                    {
                        hit.collider.gameObject.GetComponent<EnemyBehaviour>().EnemyHP -= 1;
                    }
                    else
                    {
                        Debug.Log("Weapon Invalid");
                    }

                    Debug.Log("Enemigo Tocado");
                }
            }
            if (WeaponType == 1 && AbleToShootU)
            {
                if (CanvasManager.AudioActive == true)
                {
                    RndNmb = Random.Range(0, 100);
                    if (RndNmb <= 50)
                    {
                        FindObjectOfType<Audio>().Play("UziShot");
                    }
                    else
                    {
                        FindObjectOfType<Audio>().Play("UziShot2");
                    }
                }
                AmmoUzi--;
                ClickCountU++;
                Debug.Log("Fire in hole");
            }
            if (AbleToShootU == false)
            {
                StartCoroutine(ReloadType2());
            }
            if (ClickCountU == 30)
            {
                AbleToShootU = false;
            }
        }
    }
    //-------------------------------Voids------------------------------//
    public void Shoot()
    {
        //--------------------------PistolShooting------------------------------//  
        if (ShootNextP == true &&WeaponType==0)
        {
            Ray ray = Camera.main.ViewportPointToRay(FixedCenter);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.red, 100f);

            if (AbleToShootP==true)
            {
                StartCoroutine(ShootWeapon1());
            }
            if (AbleToShootP == false)
            {
                StartCoroutine(ReloadType1());
            }
            if (ClickCountP == 12)
            {
                AbleToShootP = false;
            }
            //--------------------------EnemyHitDetection---------------------------//
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("Tocado");
                if (hit.collider.tag == "Enemy")
                {
                    if (AbleToShootP==true)
                    {
                        hit.collider.gameObject.GetComponent<EnemyBehaviour>().EnemyHP -= 4;
                    }

                    Debug.Log("Enemigo Tocado");
                }
            }
            //----------------------------------------------------------------------//
        }
        //---------------------------------------------------------------------//

        //--------------------------SniperShooting---------------------------//
        if (ShootNextS == true&&WeaponType==2)
        {
            Ray ray = Camera.main.ViewportPointToRay(FixedCenter);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 100, Color.blue, 100f);
            if (AbleToShootS==true)
            {
                StartCoroutine(ShootWeapon3());
            }
            if (AbleToShootS == false)
            {
                StartCoroutine(ReloadType3());
            }
            if (ClickCountS == 5)
            {
                AbleToShootS = false;
            }
            if (Physics.Raycast(ray, out hit))
            {
                Debug.Log("Tocado");
                if (hit.collider.tag == "Enemy")
                {
                    if (AbleToShootS == true)
                    {
                        hit.collider.gameObject.GetComponent<EnemyBehaviour>().EnemyHP -= 10;
                    }
                    Debug.Log("Enemigo Tocado");
                }
            }
           //---------------------------------------------------------------------//
        }
    }
    //--------------------------ReloadMechanic---------------------------//
    public void Reload()
    {
        if (WeaponType == 0)
        {
            AbleToShootP = false;
            StartCoroutine(ReloadType1());
        }
        if (WeaponType == 1)
        {
            AbleToShootU = false;
            StartCoroutine(ReloadType2());
        }
        if (WeaponType == 2)
        {
            AbleToShootS = false;
            StartCoroutine(ReloadType3());
        }
    }
    //---------------------------------------------------------------------//
    public void pointerDown()
    {
        stopFiring = false;
        makeFireVariableTrue();
    }

    public void pointerUp()
    {
        isFiring = false;
        stopFiring = true;
    }

    void makeFireVariableTrue()
    {
        isFiring = true;
    }

    void makeFireVariableFalse()
    {
        isFiring = false;
        if (stopFiring == false)
        {
            Invoke("makeFireVariableTrue", 0.1f);
        }
    }
    //-----------------------------------------------------------------------------------//

    //---------------------------Coroutines------------------------------//
    IEnumerator ReloadType1()
    {
        Debug.Log("Reloading Pistol");
        if (CanvasManager.AudioActive == true)
        {
            FindObjectOfType<Audio>().Play("PistolReload");
        }
        yield return new WaitForSeconds(1f);
        ClickCountP = 0;
        AbleToShootP = true;
    }
    IEnumerator ReloadType2()
    {
        Debug.Log("Reloading Uzi");
        yield return new WaitForSeconds(2.5f);
        ClickCountU = 0;
        AbleToShootU = true;
    }
    IEnumerator ReloadType3()
    {
        Debug.Log("Reloading Sniper");
        if (CanvasManager.AudioActive == true)
        {
            FindObjectOfType<Audio>().Play("SniperReload");
        }
        yield return new WaitForSeconds(3f);
        ClickCountS = 0;
        AbleToShootS = true;
    }
    //--------------------ShootingBehaviourCoroutines--------------------//
    IEnumerator ShootWeapon1()
    {
        ShootNextP = false;
        if (CanvasManager.AudioActive == true)
        {
            FindObjectOfType<Audio>().Play("PistolShot");
        }
        AmmoPistol--;
        ClickCountP++;
        yield return new WaitForSeconds(.5f);
        ShootNextP = true;
    }
    IEnumerator ShootWeapon3()
    {
        ShootNextS = false;
        if (CanvasManager.AudioActive == true)
        {
            FindObjectOfType<Audio>().Play("SniperShot");
        }
        AmmoSniper--;
        ClickCountS++;
        yield return new WaitForSeconds(1.5f);
        ShootNextS = true;
    }
}
